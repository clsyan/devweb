import datetime
from django.utils import timezone
from django.test import TestCase
from .models import Pergunta

class PerguntaTeste(TestCase):
    def test_publicada_recentemente_com_pergunta_no_futuro(self):
        
        data = timezone.now() + datetime.timedelta(seconds=1)
        pergunta_futura = Pergunta(data_publicacao = data)
        self.assertIs(pergunta_futura.publicada_recentemente(), False)

    def test_publicada_recentemente_com_data_anterior_a_24hs_no_passado(self):
        
        data = timezone.now() - datetime.timedelta(days=1, seconds=1)
        pergunta_passado = Pergunta(data_publicacao = data)
        self.assertIs(pergunta_passado.publicada_recentemente(), False)

    def test_publicada_recentemente_com_data_nas_ultimas_24hs(self):
        
        data = timezone.now()-datetime.timedelta(hours=23,minutes=59,seconds=59)
        pergunta_ok = Pergunta(data_publicacao = data)
        self.assertIs(pergunta_ok.publicada_recentemente(), True)
