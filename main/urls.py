from django.urls import path
from . import views

app_name = 'main'
urlpatterns = [
    path('', views.IndexView.as_view(), name = 'index'),
    path('enquete/<int:pk>', views.DetalhesView.as_view(), name = 'detalhes'),
    path('enquete/<int:pk>/results', views.ResultadoView.as_view(), name = 'results'),
    path('enquete/<int:id_enquete>/votacao', views.votacao, name = 'votacao'),
    path('enquete/<int:pk>/comment_detalhe', views.comment_detalhe, name = 'comment_detalhe')
]

