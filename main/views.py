from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from .models import Pergunta, Opcao, Comment

class IndexView(generic.ListView):
    def get_queryset(self):
        return Pergunta.objects.filter(
            data_publicacao__lte=timezone.now()
        ).order_by('-data_publicacao')[:5]


class DetalhesView(generic.DetailView):
    model = Pergunta
    def get_queryset(self):
        return Pergunta.objects.filter(data_publicacao__lte=timezone.now())


class ResultadoView(generic.DetailView):
    model = Pergunta
    template_name = 'main/results.html'

def votacao(request, id_enquete):
    pergunta = get_object_or_404(Pergunta, pk=id_enquete)
    try:
        opcao_selecionada = pergunta.opcao_set.get(pk=request.POST['opcao'])
    except (KeyError, Opcao.DoesNotExist):
        return render(request, 'main/details.html', {
            'pergunta': pergunta,
            'error_message': "Selecione uma opção VÁLIDA!"
        })
    else:
        opcao_selecionada.votos += 1
        opcao_selecionada.save()
        return HttpResponseRedirect(
            reverse('main:results', args=(pergunta.id,))
        )
def comment_detalhe(req, pk):
    comment = get_object_or_404(Comment, pk=pk)
    return render(req, 'main/comment_detalhe.html', {'comment':comment})