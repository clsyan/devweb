import datetime
from django.utils import timezone
from django.test import TestCase
from django.urls import reverse
from .models import Pergunta

def criar_pergunta(texto, dias):

    data = timezone.now() + datetime.timedelta(days=dias)
    return Pergunta.objects.create(texto=texto, data_publicacao=data)

class IndexViewTeste(TestCase):
    def test_sem_perguntas_cadastradas(self):
        
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "Não há enquetes cadastradas até o momento!")
        self.assertQuerysetEqual(resposta.context['pergunta_list'], [])

    def test_com_pergunta_no_passado(self):
        
        criar_pergunta(texto='Pergunta no passado', dias=-30)
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "Pergunta no passado")
        self.assertQuerysetEqual(resposta.context['pergunta_list'], ['<Pergunta: Pergunta no passado>'])

    def test_com_pergunta_no_passado(self):
        
        criar_pergunta(texto='Pergunta no passado', dias=-30)
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "Pergunta no passado")
        self.assertQuerysetEqual(
            resposta.context['pergunta_list'], ['<Pergunta: Pergunta no passado>']
    )



    def test_pergunta_no_passado_e_outra_no_futuro(self):
        
        criar_pergunta(texto="Pergunta no passado", dias=-1)
        criar_pergunta(texto="Pergunta no futuro", dias=1)
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "Pergunta no passado")
        self.assertQuerysetEqual(resposta.context['pergunta_list'],
            ['<Pergunta: Pergunta no passado>']
    )


    def test_duas_perguntas_no_passado(self):
        
        criar_pergunta(texto="Pergunta no passado 1", dias=-1)
        criar_pergunta(texto="Pergunta no passado 2", dias=-5)
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "Pergunta no passado")
        self.assertQuerysetEqual(resposta.context['pergunta_list'], ['<Pergunta: Pergunta no passado 1>', '<Pergunta: Pergunta no passado 2>'])
