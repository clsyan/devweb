import datetime
from django.utils import timezone
from django.test import TestCase
from django.urls import reverse
from .models import Pergunta

def criar_pergunta(texto, dias):
    """
    Função para criação de um pergunta para texto e uma variação de dias
    """
    data = timezone.now() + datetime.timedelta(days=dias)
    return Pergunta.objects.create(texto=texto, data_publicacao=data)

class DetalhesViewTeste(TestCase):
    def test_id_invalido(self):
        
        resposta = self.client.get(
            reverse('main:detalhes', args=[99,])
        )
        self.assertEqual(resposta.status_code, 404)

    def test_pergunta_no_futuro(self):
        
        pergunta_futura = criar_pergunta(texto="Pergunta no futuro", dias=5)
        resposta = self.client.get(
            reverse('main:detalhes', args=[pergunta_futura.id,])
        )
        self.assertEqual(resposta.status_code, 404)

    def test_pergunta_no_passado(self):
        
        pergunta_passada = criar_pergunta(texto="Pergunta no passado", dias=-1)
        resposta = self.client.get(
            reverse('main:detalhes', args=[pergunta_passada.id,])
        )
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, pergunta_passada.texto)